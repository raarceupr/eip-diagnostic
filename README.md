### Repositorio para preguntas diagnosticas.

--

#### Instructions

1. To start a C++ project, press the `New Project` button or go to the Qt main menu and in `File` select `New File or Project`. A window similar to the one in Figure 2 will appear. Select `Non-Qt Project`, `Plain C++ Project` and press `Choose`.

    ![figure2.png](images/figure2.png)
   
    **Figure 2.** Start C++ project without graphical applications.
 <center>

2. Write the name of the project, select the directory where you would like to save it, press `Next` in that window and the following one, and then `Finish` in the next one.

    This process will create a new Qt project with the skeleton for a basic C++ program that only displays "Hello World!". Before continuing, select `Projects` in the vertical menu to the left. A window called `Build Settings` should appear. In this window, make sure the `Shadow build` checkbox is NOT selected, as shown in Figure 3.
 
    ![figure3.png](images/figure3.png)
 
    **Figure 3.** The `Shadow build` option is not selected.
    
1. sdsds