#include <iostream>
#include <cmath>
using namespace std;

class Point {
private:
  int x,y;
public:
  Point(int xx, int yy) { x = xx; y =yy;}

  bool isFirstQ() {
      if (x > 0 && y > 0) return true;
      else false;
  }

  float distanceToPoint(const Point &p) {
    return sqrt( pow(p.x - x, 2.) +  pow(p.y - y, 2.));
  }

};

int main() {
    Point p(9,10);
    Point p2(9,12);
    cout << p.isFirstQ() << endl;
    cout << p.distanciaAPunto(p2) << endl;

}
